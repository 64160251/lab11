public class App {
    public static void main(String[] args) throws Exception {
        Bat bat1 = new Bat("Bat");
        Fish fish1 = new Fish("Fish");
        Plane plane1 = new Plane("AirLine", "Super Engine");
        Bird bird1 = new Bird("Eka");
        Cat cat1 = new Cat("Bunmee");
        Crocodile crocodile1 = new Crocodile("Natacha");
        Dog dog1 = new Dog("Boo");
        Human human1 = new Human("Aun");
        Rat rat1 = new Rat("Jerry");
        Snake snake1 = new Snake("Prayanak");
        Submarine submarine1 = new Submarine("999999997", "lottery");

        Flyable[] flyableObjects = {bat1,plane1,bird1,};
        for(int i =0;i<flyableObjects.length;i++){
            flyableObjects[i].takeoff();
            flyableObjects[i].fly();
            flyableObjects[i].landing();
        }

        Swimable[] swimableObjects = {crocodile1,fish1,submarine1};
        for(int i =0;i<swimableObjects.length;i++){
            swimableObjects[i].swim();
        }

        Walkable[] walkableObjects = {cat1,dog1,rat1,human1};
        for(int i =0;i<walkableObjects.length;i++){
            walkableObjects[i].Walk();
            walkableObjects[i].run();
        }

        Crawlable[] crawlableObjects = {crocodile1,snake1};
        for(int i =0;i<crawlableObjects.length;i++){
            crawlableObjects[i].Crawl();
        }

        Animal[] animalObjects = {bat1,bird1,cat1,crocodile1,dog1,fish1,human1,rat1,snake1,};
        for(int i =0;i<animalObjects.length;i++){
            animalObjects[i].eat();
            animalObjects[i].sleep();
        }
    }
}
